
<div style="width:650px; margin:0px auto; text-align:justify;">
<b style="color:#FF0000;">MITOS2 can be found <a href="http://mitos2.bioinf.uni-leipzig.de/">here</a>!</b>
<br><br>

<b>Citation</b>:

<i>M. Bernt, A. Donath, F. Jühling, F. Externbrink, C. Florentz, G. Fritzsch, J. Pütz, M. Middendorf, P. F. Stadler</i><br>
<b>MITOS: Improved de novo Metazoan Mitochondrial Genome Annotation</b><br>
Molecular Phylogenetics and Evolution 2013, 69(2):313-319 <a href="http://dx.doi.org/10.1016/j.ympev.2012.08.023">link</a>
</div> 

